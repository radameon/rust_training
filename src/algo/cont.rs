use std::vec;

pub mod cont {
    // TODO: make container generic
    /// Return a max element from a container
    /// 
    /// # Examples
    /// ```
    /// let v = vec![1, 2, 3];
    /// let max_elem = algo::algo::max_element(&v);
    /// 
    /// assert_eq(max_elem, 3);
    /// ```
    pub fn max_element<T: Eq + Ord>(container : &[T]) -> &T {
        let mut max = &container[0];

        for item in container {
            if item > max {
                max = item;
            }
        }

        max
    }

    pub fn min_element<T: Eq + Ord >(container : &[T]) -> &T {
        let mut min = &container[0];

        for item in container {
            if item < min {
                min = item;
            }
        }

        min
    }

}

mod tests {
    use super::*;

    #[test]
    fn find_min() {
        let v = vec![1, 2, 3];
        let min_elem = cont::min_element(&v);

        assert_eq!(min_elem.clone(), 1);
    }
}