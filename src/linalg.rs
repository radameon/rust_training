pub mod vector;

pub mod linalg {
    pub trait TwoDimensional<T> {
        fn x(&self) -> T;
        fn y(&self) -> T;
    }

    pub trait ThreeDimensional<T> {
        fn z(&self) -> T;
    }

    pub trait FourDimensional<T> {
        fn w(&self) -> T;
    }

    pub trait LinAlgVector<T> {
        fn length(&self) -> T;
        fn dot<O>(&self, other : &O ) -> T;
    }

    //pub trait FourDimensionalVector<T>: TwoDimensional<T> + ThreeDimensional<T> + FourDimensional<T> + LinAlgVector<T>{}
}