pub mod aggregator;

pub mod aggr {
    pub trait Summary {
        fn summarize(&self) -> String {
            String::from("Read more...")
        }
    }
}