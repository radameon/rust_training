//! # Useful RUST snippets
//! 
//! Just a bunch of code, I wrote while trying to learn the language

use std::convert::TryInto;
use std::fs::File;
use std::io::ErrorKind;
use std::io::{self, Read};
use std::string;
use std::collections::HashMap;
use aggr::aggr::Summary;
use linalg::vector::vector::Vector4;
use aggr::aggregator::aggregator::{NewsArticle, Tweet};
use game::units::units::{StrategyGameKnight, StrategyGameFighter, StrategyGameWorker};
//use game::structures::structures::Tower;
use inv::inverntory;
use std::thread;
use std::time::Duration;

use crate::linalg::vector;
use crate::aggr::aggregator;
use crate::algo::cont;
use crate::game::game::StrategyGameActor;
//use crate::game::structures;
use crate::game::units;
use crate::inv::inverntory::inv::{Inventory, ShirtColor};
use crate::it::shoe::shoe;

use game::Tower;

pub mod linalg;
pub mod algo;
pub mod aggr;
pub mod game;
pub mod inv;
pub mod it;

fn print_n_array(arr : [u32; 10], number : u32) -> bool {
    let mut i : usize = 0;
    let size = arr.len() as u32;
    if number > size {
        return false;
    }

    for a in arr {
        println!("Number is {a}");
    }

    true
}

fn print_array(arr : [u32; 10]) {
    print_n_array(arr, 10);
}

fn calculate_str_len(s: &String) -> usize {
    s.len()
}

fn append_to_str(s: &mut String, addendum: String) {
    s.push_str(&addendum);
}

fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

#[derive(Debug)]
struct User {
    active: bool,
    username: String,
    email: String,
    logonsc: usize,
}

struct Vectori32 (i32, i32, i32);

impl Vectori32 {
    fn length(&self) -> f32 {
        ((self.0 * self.0 + self.1 * self.1 + self.2 * self.2) as f32).sqrt()
    }

    fn dot(&self, other : Vectori32) -> f32 {
        (self.0 * other.0 + self.1 * other.1 + self.2 * other.2) as f32
    }

    fn new() -> Self {
        Vectori32 {
            0:0, 1:0, 2:0
        }
    }
}

enum IPAddrType {
    IPv4(u8, u8, u8, u8),
    IPv6(String)
}

impl IPAddrType {
    fn call(&self) {
        println!("I'm helping!");
    }
}

fn wrapped_division(val : f32, divisor : f32) -> Option<f32> {
    if divisor == 0.0 {
        return None;
    }

    Some(val / divisor)
}

fn print_option(opt : Option<f32>) {
    match opt {
        None => println!("Option value is not set!"),
        Some(v) => println!("Values is {}", v),
    }
}

fn feeling_lucky(v : u32) {
    match v {
        7 => println!("Lucky seven!"),
        _ => println!("Not this time. Better luck!"),
    }
}

fn update_or_create_user(username : String, pass : String, container : &mut HashMap<String, String>) {
    let user = container.entry(username).or_insert(pass.clone());
    *user = pass;
}

enum SpreadSheetCell {
    Int(i32),
    Float(f32),
    Text(String),
}

fn off_by_one_panic() {
    let v = vec![0, 1, 2, 3];
    v[99];
}

fn read_user_from_file(file_path : String, short_way : bool) -> Result<String, io::Error> {
    if short_way {
        std::fs::read_to_string(file_path)
    } else {
        let mut name : String = String::new();
        File::open(file_path)?.read_to_string(&mut name)?;
        Ok(name)
    }
}

fn main() {
    let mut x : i32 = 5;
    println!("X is {x}");

    {
        let mut x: i32 = 112;
        println!("X inside the inner scope is {x}");
    }

    x = 6;
    println!("X is {x}");

    let a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    print_array(a);

    let printed_success = print_n_array(a, 5);

    if printed_success {
        println!("Successfully printed 5 numbers!");
    }

    let mut str : String = String::from("Testing");
    let mut len : usize = calculate_str_len(&str);

    println!("Testing string len {len}");

    let addendum = String::from(" this shit");

    append_to_str(&mut str, addendum);

    len = calculate_str_len(&str);

    println!("String {str} of len {len}");

    let found_word = first_word(&str);

    println!("First word is {found_word}");

    let mut admin = User {
        active: true,
        username: String::from("root"),
        email: String::from("root@domain.commm"),
        logonsc: 0,
    };

    println!("test {:?}", admin);

    let vec = Vectori32(3, 2, 3);

    println!("vector len {}", vec.length());

    let some_new_vec = Vectori32::new();

    let home = IPAddrType::IPv4(127, 0, 0, 1);
    let loopback = IPAddrType::IPv6(String::from("localhost"));
    loopback.call();

    let div_res = wrapped_division(12., 2.);
    print_option(div_res);

    feeling_lucky(1);
    feeling_lucky(7);

    let tv:Vector4<i32> = Vector4::new(1, 2, 3, 4);

    println!("Printing test vector ({}, {}, {}, {})", tv.x(), tv.y(), tv.z(), tv.w());

    let mut test_vec = vec![1, 2, 3, 4];

    test_vec.push(5);
    test_vec.push(6);
    test_vec.push(7);

    let third_elem = &test_vec[2];
    println!("Third element in a vector is {third_elem}");

    let eight_element  = test_vec.get(7);

    match eight_element {
        None => println!("No eighth elemnt in vector"),
        Some(eight_element) => println!("Eighth element is {eight_element}"),
    }

    let rows = vec![
        SpreadSheetCell::Int(3),
        SpreadSheetCell::Float(1.0),
        SpreadSheetCell::Text(String::from("Something"))
    ];

    let mut users : HashMap<String, String> = HashMap::new();
    users.insert(String::from("admin"), String::from("admin"));
    users.insert(String::from("moderator"), String::from("zalupa213!"));
    users.insert(String::from("vasya"), String::from("1234"));

    println!("Initial users map {:?}", users);

    update_or_create_user(String::from("vasya"), String::from("hackerproof#goodpass!"), &mut users);

    println!("Trying to update vasya {:?}", users);

    update_or_create_user(String::from("akakiy"), String::from("iamacoolpassword!123"), &mut users);

    println!("Trying to update missing user {:?}", users);

    println!("Hello, world!");

    //panic!("FUCK THAT ALL!!");

    //off_by_one_panic();

    let fpath : String = String::from("test.txt");

    let greeting_file_result = File::open(&fpath);

    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(e) => match e.kind() {
            ErrorKind::NotFound => match File::create(fpath) {
                Ok(f) => f,
                Err(er) => panic!("Can't create file {:?}", er),
            },
            other_error=> {
              panic!("Unrecoverable fs error {:?}", other_error);
            },
        },
    };

    /*let read_file_result = read_user_from_file(String::from("/home/rust_oper/user.txt"), false);
    match read_file_result {
        Ok(V) => println!("Username is {V}"),
        Err(e) => panic!("Error while reading the file {:?}", e),
    }*/

    let tweet = Tweet {
        username: String::from("Sucka"),
        content: String::from("Tweetter is for pussies!"),
        reply: false,
        retweet: false,
    };

    /// Traits test on strategy game begin
    {
        // first player units
        let mut w11 = StrategyGameWorker::new(1);
        let f11 = StrategyGameFighter::new(1);
        let f12 = StrategyGameFighter::new(1);
        let k11 = StrategyGameKnight::new(1);

        // second player units
        let w21 = StrategyGameWorker::new(2);
        let w22 = StrategyGameWorker::new(2);
        let f21 = StrategyGameFighter::new(2);
        let f22 = StrategyGameFighter::new(2);
        let k21 = StrategyGameKnight::new(2);

        game::game::actor_make_action(&w11);
        game::game::actor_make_action(&k21);
        //actor_make_action(&21); // compilation error

        game::game::damage_civllian_special(&k21, &mut w11);

        let mut t  = Tower::new(&k11);
        t.enter_garrison(&k21);
    }
    /// Traits test on strategy game end

    println!("1 new tweet {}", tweet.summarize());

    let store = Inventory {
        shirts: vec![ShirtColor::Blue, ShirtColor::Red, ShirtColor::Blue],
    };

    let user_perf1 = Some(ShirtColor::Red);
    let giveaway1 = store.giveaway(user_perf1);

    println!("The user with preference {:?} gets {:?}", user_perf1, giveaway1);

    let user_perf2 = None;
    let giveaway2 = store.giveaway(user_perf2);

    println!("The user with preference {:?} gets {:?}", user_perf2, giveaway2);

    let expensive_closure = |num: u32| -> u32 {
        println!("Calculating slowly!");
        thread::sleep(Duration::from_secs(2));
        num
    };

    {
        let list = vec![1, 2, 3];
        println!("Before defining closure {:?}", list);

        thread::spawn(move || println!("From thread {:?}", list))
            .join()
            .unwrap();
    }

    // closure borrows immutably
    {
        let list = vec![1, 2, 3];
        println!("Before defining closure {:?}", list);

        let only_borrows = || println!("From closure {:?}", list);

        println!("Before calling closure {:?}", list);
        only_borrows();
        println!("After calling closure {:?}", list);
    }

    // closure borrows mutably
    {
        let mut list = vec![1, 2, 3];
        println!("Before defining closure {:?}", list);

        let mut borrows_mutably = || list.push(7);

        borrows_mutably();
        println!("After calling closure {:?}", list);
    }

    // iterators
    {
        let v1 = vec![1, 2, 3];
        let v1_it = v1.iter();

        for val in v1_it {
            println!("Got: {}", val);
        }
    }

    {
        
        
    }
}