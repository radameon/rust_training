pub mod structures {
    use crate::game::game::{StrategyActorArmored, StrategyActorDamage};

    pub struct Tower<'a, T> {
        id: u32,
        health: u32,
        protection: u32,
        garrison: &'a T,
        garrison_full: bool,
    }

    impl<'a, T> Tower<'a, T> {
        pub fn new(g: &'a T) -> Self {
            Self {
                id: 0,
                health: 500,
                protection: 100,
                garrison: g,
                garrison_full: true,
            }
        }

        pub fn enter_garrison(&mut self, g: &'a T) {
            if self.garrison_full {
                println!("Garrison {} is full", self.id);
                return;
            }
            self.garrison = &g;
            self.garrison_full = true;
        }

        pub fn leave_garrison(&mut self) -> Option<&T> {
            if self.garrison_full {
                self.garrison_full = false;
                return Some(self.garrison);
            } else {
                None
            }
        }
    }

    impl<'a, T: StrategyActorDamage> Tower<'a, T> {
        pub fn increase_defence(&self, target: &mut impl StrategyActorArmored) {
            target.increase_armor(self.protection);
        }

        pub fn decrease_defence(&self, target: &mut impl StrategyActorArmored) {
            target.decrease_armor(self.protection);
        }
    }
}