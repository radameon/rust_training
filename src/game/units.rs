pub mod units {
    use crate::game::game::{StrategyActorArmored, StrategyActorDamage, StrategyCivilActor, StrategyGameActor, StrategySpecialActor};
    use rand::Rng;

    pub struct StrategyGameWorker {
        id: u32,
        health: u32,
        player: u8,
    }

    pub struct StrategyGameFighter {
        id: u32,
        health: u32,
        damage: u32,
        player: u8,
    }

    pub struct StrategyGameKnight {
        id: u32,
        health: u32,
        damage: u32,
        ultra_modifier: u32,
        armor_reduction: u32,
        player: u8,
    }

    impl StrategyGameWorker {
        pub fn new(player: u8) -> Self {
            let mut gen = rand::thread_rng();
            StrategyGameWorker {
                id: gen.gen_range(0..u32::MAX),
                health: 100,
                player,
            }
        }
    }

    impl StrategyGameActor for StrategyGameWorker {
        fn get_my_type(&self) -> String {
            return String::from("Worker");
        }

        fn get_health(&self) -> u32 {
            return self.health;
        }
    }

    impl StrategyCivilActor for StrategyGameWorker {
        fn build(&self) {
            println!("I am building something");
        }
    }

    impl StrategyActorDamage for StrategyGameWorker {
        fn receive_damage(&mut self, damage: u32) {
            if damage < self.health {
                self.health -= damage;
            } else {
                self.health = 0;
            }
        }

        fn deal_damage(&self, target: &mut impl StrategyActorDamage) {
            println!("I can't damage! I'm unarmed!");
        }
    }

    impl StrategyGameFighter {
        pub fn new(player: u8) -> Self {
            let mut gen = rand::thread_rng();
            StrategyGameFighter {
                id: gen.gen_range(0..u32::MAX),
                health: 150,
                damage: 20,
                player,
            }
        }
    }

    impl StrategyGameActor for StrategyGameFighter {
        fn get_my_type(&self) -> String {
            return String::from("Fighter");
        }

        fn get_health(&self) -> u32 {
            return self.health;
        }
    }

    impl StrategyActorDamage for StrategyGameFighter {
        fn receive_damage(&mut self, damage: u32) {
            if damage < self.health {
                self.health -= damage;
            } else {
                self.health = 0;
            }
        }

        fn deal_damage(&self, target: &mut impl StrategyActorDamage) {
            target.receive_damage(self.damage);
        }
    }

    impl StrategyGameKnight {
        pub fn new(player: u8) -> Self {
            let mut gen = rand::thread_rng();
            StrategyGameKnight {
                id: gen.gen_range(0..u32::MAX),
                health: 225,
                damage: 35,
                ultra_modifier: 2,
                armor_reduction: 5,
                player,
            }
        }
    }

    impl StrategyGameActor for StrategyGameKnight {
        fn get_my_type(&self) -> String {
            return String::from("Knight");
        }

        fn get_health(&self) -> u32 {
            return self.health;
        }
    }

    impl StrategySpecialActor for StrategyGameKnight {
        fn special(&self, target: &mut impl StrategyActorDamage) {
            let total_damage = self.calculate_special_damage();
            target.receive_damage(total_damage);
        }

        fn calculate_special_damage(&self) -> u32 {
            self.damage * self.ultra_modifier
        }
    }

    impl StrategyActorDamage for StrategyGameKnight {
        fn receive_damage(&mut self, damage: u32) {
            if damage < self.health {
                self.health -= damage;
            } else {
                self.health = 0;
            }
        }

        fn deal_damage(&self, target: &mut impl StrategyActorDamage) {
            target.receive_damage(self.damage);
        }
    }

    impl StrategyActorArmored for StrategyGameKnight {
        fn increase_armor(&mut self, amount: u32) {
            self.armor_reduction += amount;
        }

        fn decrease_armor(&mut self, amount: u32) {
            self.armor_reduction -= amount;
        }
    }

}