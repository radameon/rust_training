#[cfg(test)]
mod tests {
    use crate::game::game::{StrategyActorDamage, StrategyGameActor, StrategySpecialActor};
    use crate::game::units::units::*;

    #[test]
    fn test_damage_civ() {
        let mut knight : StrategyGameKnight = StrategyGameKnight::new(1);
        let mut worker : StrategyGameWorker = StrategyGameWorker::new(1);

        assert_eq!(worker.get_health(), 100);

        knight.deal_damage(&mut worker);

        assert_eq!(worker.get_health(), 65);
    }

    #[test]
    fn test_damage_special() {
        let mut knight : StrategyGameKnight = StrategyGameKnight::new(1);
        let mut fighter : StrategyGameFighter = StrategyGameFighter::new(1);

        assert_eq!(fighter.get_health(), 150);

        knight.special(&mut fighter);

        assert_eq!(fighter.get_health(), 80);
    }

    // don't want to create one more lib just for iterators
    // so putting these tests here
    #[test]
    fn iterator_demonstration() {
        let v1 = vec![1, 2, 3];

        let mut v1_it = v1.iter();

        assert_eq!(v1_it.next(), Some(&1));
        assert_eq!(v1_it.next(), Some(&2));
        assert_eq!(v1_it.next(), Some(&3));
        assert_eq!(v1_it.next(), None);
    }

    // iterator consume
    #[test]
    fn iterator_sum() {
        let v1 = vec![1, 2, 3];
        let v1_it = v1.iter();
        let total: i32 = v1_it.sum();

        assert_eq!(total, 6);
    }

    #[test]
    fn transform_iter() {
        let v1 : Vec<i32> = vec![1, 2, 3];
        let v2 : Vec<_> = v1.iter().map(|x| x + 1).collect();

        assert_eq!(v2, vec![2, 3, 4]);
    }
}