pub mod vector {

    #[derive(Debug)]
    pub struct Vector4<T> (T, T, T, T);

    impl<T> Vector4<T> {
        pub fn new(x:T , y:T, z:T, w:T) -> Self {
            return Vector4(x, y, z, w);
        }

        pub fn x(&self) -> &T {
            return &self.0;
        }

        pub fn y(&self) -> &T {
            return &self.1;
        }

        pub fn z(&self) -> &T {
            return &self.2;
        }

        pub fn w(&self) -> &T {
            return &self.3;
        }
    }
}