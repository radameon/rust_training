//! A simple startegy game module
//! 
//! Nothing special, it's just that training is easier ina form of a game

pub use self::game::StrategyActorArmored;
pub use self::game::StrategyActorDamage;
pub use self::units::units::StrategyGameFighter;
pub use self::units::units::StrategyGameKnight;
pub use self::units::units::StrategyGameWorker;
pub use self::structures::structures::Tower;

pub mod units;
pub mod structures;
mod tests;

pub mod game {
    pub trait StrategyGameActor {
        fn get_my_type(&self) -> String;

        fn act(&self) {
            println!("Just doing my work as {}", self.get_my_type());
        }

        fn get_health(&self) -> u32;
    }

    pub trait StrategyCivilActor {
        fn build(&self);
    }

    pub trait StrategySpecialActor {
        fn special(&self, target: &mut impl StrategyActorDamage);
        fn calculate_special_damage(&self) -> u32;
    }

    pub trait StrategyUltActor {
        fn ultimate(&self);
    }

    pub trait StrategyActorDamage {
        fn receive_damage(&mut self, damage: u32);
        fn deal_damage(&self, target: &mut impl StrategyActorDamage);
    }

    pub trait StrategyActorArmored {
        fn increase_armor(&mut self, amount: u32);
        fn decrease_armor(&mut self, amount: u32);
    }

    pub fn actor_make_action<T: StrategyGameActor>(a : &T) {
        a.act();
    }

    pub fn damage_civllian_special<T, U>(source: &T, target: &mut U)
    where
        T: StrategySpecialActor + StrategyActorDamage + StrategyGameActor,
        U: StrategyCivilActor + StrategyActorDamage + StrategyGameActor,
    {
        println!("Actor of type {} is performing a special attack on actor of type {}", source.get_my_type(), target.get_my_type());
        source.deal_damage(target);
        println!("Total damage dealt {}, remaining civilian health {}", source.calculate_special_damage(), target.get_health());
    }
}